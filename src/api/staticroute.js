import request from '@/utils/request'

//新增
export function staticrouteAdd(data) {
  return request({
    url: '/lbm/staticRoute/addStaticRouteEntity',
    method: 'post',
    data
  })
}

//修改
export function staticrouteEdit(data) {
  return request({
    url: '/lbm/staticRoute/editStaticRouteEntity',
    method: 'put',
    data
  })
}

//获取列表
export function staticrouteList(data) {
  return request({
    url: '/lbm/staticRoute/getStaticRouteVoPage',
    method: 'get',
    params: data
  })
}

//获取某一个
export function staticrouteById(id) {
  return request({
    url: '/lbm/staticRoute/getStaticRouteVoInfoById/'+id,
    method: 'get',
  })
}

//删除（一个）
export function deleteStaticRoute(id) {
  return request({
    url: '/lbm/staticRoute/delStaticRouteVoInfoById/'+id,
    method: 'delete',
  })
}

//删除（多个）
export function delDevStaticRoutes(id) {
  return request({
    url: '/lbm/staticRoute/delStaticRouteVoInfoByIds/'+id,
    method: 'delete',
  })
}

