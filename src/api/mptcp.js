import request from '@/utils/request'

//新增
export function proxyAdd(data) {
  return request({
    url: '/lbm/proxy/addMptcpProxyEntity',
    method: 'post',
    data
  })
}

//修改
export function proxyEdit(data) {
  return request({
    url: '/lbm/proxy/editMptcpProxyEntity',
    method: 'post',
    data
  })
}

//获取列表
export function proxyList(data) {
  return request({
    url: '/lbm/proxy/getMptcpProxyVoPage/',
    method: 'get',
    params: data
  })
}

//删除（一个）
export function deleteProxy(id) {
  return request({
    url: '/lbm/proxy/delMptcpProxyVoInfoById/'+id,
    method: 'delete',
  })
}

//删除（多个）
export function delDevMptcpProxys(id) {
  return request({
    url: '/lbm/proxy/delMptcpProxyVoInfoByIds/'+id,
    method: 'delete',
  })
}

//获取列表
export function mptcpList(data) {
  return request({
    url: '/lbm/mptcp/getMptcpVoPage/',
    method: 'get',
    data
  })
}

//新增
export function mptcpAdd(data) {
  return request({
    url: '/lbm/mptcp/addMptcpEntity',
    method: 'post',
    data
  })
}

//修改
export function mptcpEdit(data) {
  return request({
    url: '/lbm/mptcp/editMptcpEntity',
    method: 'put',
    data
  })
}
//删除（一个）
export function deletemptcp(id) {
  return request({
    url: '/lbm/mptcp/delMptcpVoInfoById/'+id,
    method: 'delete',
  })
}
