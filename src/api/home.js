import request from '@/utils/request'

export function login(data) {
  return request({
	url:'/lbm/home/login',
    method: 'post',
    params:data,
  })
}

export function getInfo(token) {
  return request({
	url:'/lbm/home/userInfo',
    method: 'get',
    params: { token }
  })
}
export function getCpuAndMemoryUsage(data) {
  return request({
	  url:'/lbm/home/getCpuAndMemoryUsage',
    method: 'get',
    params:data
  })
}

export function getInDeviceFlowChart(data) {
  return request({
    url:'/lbm/home/getInDeviceFlowChart',
    method: 'post',
    data
  })
}

export function getoutDeviceFlowChart(data) {
  return request({
    url:'/lbm/home/getoutDeviceFlowChart',
    method: 'post',
    data
  })
}
