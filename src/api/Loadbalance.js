import request from '@/utils/request'

//新增
export function lbproxyAdd(data) {
  return request({
    url: '/lbm/lbproxy/addLoadBalanceProxyEntity',
    method: 'post',
    data
  })
}

//修改
export function lbproxyEdit(data) {
  return request({
    url: '/lbm/lbproxy/editLoadBalanceProxyEntity',
    method: 'post',
    data
  })
}

//获取列表
export function lbproxyList(data) {
  return request({
    url: '/lbm/lbproxy/getLoadBalanceProxyVoPage/',
    method: 'get',
    params: data
  })
}

//删除（一个）
export function deleteProxy(id) {
  return request({
    url: '/lbm/lbproxy/delLoadBalanceProxyVoInfoById/'+id,
    method: 'delete',
  })
}

//删除（多个）
export function delDevLoadBalanceProxys(id) {
  return request({
    url: '/lbm/lbproxy/delLoadBalanceProxyVoInfoByIds/'+id,
    method: 'delete',
  })
}

//获取列表
export function loadbalanceList(data) {
  return request({
    url: '/lbm/loadbalance/getLoadBalanceVoPage/',
    method: 'get',
    data
  })
}

//新增
export function loadbalanceAdd(data) {
  return request({
    url: '/lbm/loadbalance/addLoadBalanceEntity',
    method: 'post',
    data
  })
}

//修改
export function loadbalanceEdit(data) {
  return request({
    url: '/lbm/loadbalance/editLoadBalanceEntity',
    method: 'put',
    data
  })
}
//删除（一个）
export function deleteloadbalance(id) {
  return request({
    url: '/lbm/loadbalance/delLoadBalanceVoInfoById/'+id,
    method: 'delete',
  })
}
