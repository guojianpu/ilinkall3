import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

/* Layout */
import Layout from '@/layout'

/**
 * Note: sub-menu only appear when route children.length >= 1
 * Detail see: https://panjiachen.github.io/vue-element-admin-site/guide/essentials/router-and-nav.html
 *
 * hidden: true                   if set true, item will not show in the sidebar(default is false)
 * alwaysShow: true               if set true, will always show the root menu
 *                                if not set alwaysShow, when item has more than one children route,
 *                                it will becomes nested mode, otherwise not show the root menu
 * redirect: noRedirect           if set noRedirect will no redirect in the breadcrumb
 * name:'router-name'             the name is used by <keep-alive> (must set!!!)
 * meta : {
    roles: ['admin','editor']    control the page roles (you can set multiple roles)
    title: 'title'               the name show in sidebar and breadcrumb (recommend set)
    icon: 'svg-name'/'el-icon-x' the icon show in the sidebar
    breadcrumb: false            if set false, the item will hidden in breadcrumb(default is true)
    activeMenu: '/example/list'  if set path, the sidebar will highlight the path you set
  }
 */

/**
 * constantRoutes
 * a base page that does not have permission requirements
 * all roles can be accessed
 */
export const constantRoutes = [
  {
    path: '/login',
    component: () => import('@/views/login/index'),
    hidden: true
  },

  {
    path: '/404',
    component: () => import('@/views/404'),
    hidden: true
  },

  {
    path: '/',
    component: Layout,
    redirect: '/dashboard/jiekou',
    name: 'Dashboard',
    meta: { title: '设备详情', icon: 'home' },
    children: [{
        path: '/dashboard/jiekou',
        name: 'jiekou',
        component: () => import('@/views/dashboard/jiekou'),
        meta: { title: '设备接口图' }
      },{
        path: '/dashboard/cpu',
        name: 'cpu',
        component: () => import('@/views/dashboard/cpu'),
        meta: { title: 'CPU和内存状态' }
    }]
  },
  {
    path: '/equipment',
    component: Layout,
    meta: { title: '设备设置', icon: 'base' },
    children: [
      {
        path: 'equipment',
        name: 'equipment',
        component: () => import('@/views/equipment/index'),
        meta: { title: '基本设置'}
      },
      {
        path: 'balancing',
        name: 'balancing',
        component: () => import('@/views/balancing/index'),
        meta: { title: '负载均衡设置'}
      }
    ]
  },
  {
    path: '/base',
    hidden: true,
    component: Layout,
    children: [
      {
        path: 'jiekou',
        name: 'jiekou',
        component: () => import('@/views/base/index'),
        meta: { title: '接口配置',activeMenu:'/equipment/equipment' }
      },
      {
        path: 'jtly',
        name: 'jtly',
        component: () => import('@/views/base/jtly'),
        meta: { title: '静态路由配置',activeMenu:'/equipment/equipment' }
      },
      {
        path: 'yingyong',
        name: 'yingyong',
        component: () => import('@/views/base/yingyong'),
        meta: { title: '应用配置',activeMenu:'/equipment/equipment' }
      },
      {
        path: 'liuzhu',
        name: 'liuzhu',
        component: () => import('@/views/base/liuzhu'),
        meta: { title: '流组配置',activeMenu:'/equipment/equipment' }
      },
      {
        path: 'qos',
        name: 'qos',
        component: () => import('@/views/base/qos'),
        meta: { title: 'qos配置',activeMenu:'/equipment/equipment' }
      }
    ]
  },
  {
    path: '/balancing',
    component: Layout,
    hidden: true,
    children: [
      {
        path: 'mptcp',
        name: 'mptcp',
        component: () => import('@/views/balancing/mptcp'),
        meta: { title: 'mptcp',activeMenu:'/equipment/balancing'}
      },
      {
        path: 'wuyuanzhu',
        name: 'wuyuanzhu',
        component: () => import('@/views/balancing/wuyuanzhu'),
        meta: { title: 'wuyuanzhu',activeMenu:'/equipment/balancing'}
      }
    ]
  },
  {
    path: '/bag',
    component: Layout,
    meta: { title: '工具', icon: 'bag' },
    children: [
      {
        path: 'tj',
        name: 'tj',
        component: () => import('@/views/dashboard/index'),
        meta: { title: '流量统计' }
      },
      {
        path: 'bag',
        name: 'bag',
        component: () => import('@/views/bag/index'),
        meta: { title: '抓包' }
      },
    ]
  },
  {
    path: '/log',
    component: Layout,
    children: [
      {
        path: 'log',
        name: 'log',
        component: () => import('@/views/log/index'),
        meta: { title: '日志', icon: 'log' }
      }
    ]
  },


  // 404 page must be placed at the end !!!
  { path: '*', redirect: '/404', hidden: true }
]

const createRouter = () => new Router({
  // mode: 'history', // require service support
  scrollBehavior: () => ({ y: 0 }),
  routes: constantRoutes
})

const router = createRouter()

// Detail see: https://github.com/vuejs/vue-router/issues/1234#issuecomment-357941465
export function resetRouter() {
  const newRouter = createRouter()
  router.matcher = newRouter.matcher // reset router
}

export default router
